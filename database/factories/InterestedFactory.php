<?php

namespace Database\Factories;

use App\Models\Cake;
use App\Models\Interested;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Interested>
 */
class InterestedFactory extends Factory
{
    protected $model = Interested::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'email' => $this->faker->email(),
            'cake_id' => Cake::factory()->create()
        ];
    }
}
