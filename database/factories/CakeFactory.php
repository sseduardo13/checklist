<?php

namespace Database\Factories;

use App\Models\Cake;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cake>
 */
class CakeFactory extends Factory
{
    protected $model = Cake::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'weight' => $this->faker->numberBetween(100, 2000),
            'value' => $this->faker->numberBetween(10, 999),
            'quantity' => $this->faker->numberBetween(1, 50)
        ];
    }
}
