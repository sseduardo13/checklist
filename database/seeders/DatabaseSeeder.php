<?php

namespace Database\Seeders;

use App\Models\Cake;
use App\Models\Interested;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $cake = Cake::create([
            'name' => 'Chocolate',
            'weight' => 1000,
            'value' => 99.75,
            'quantity' => 5
        ]);

        for ($i=1; $i <= 3; $i++) { 
            Interested::create([
                'email' => "interested{$i}@mail.com",
                'cake_id' => $cake->id
            ]);
        }
    }
}
