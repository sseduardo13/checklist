# Desafio Checklist Fácil

### Descrição:
O teste é composto de um CRUD para bolos e cadastro de emails interessados para cada bolo cadastrado. O sistema dispara um email para cada interessado quando um bolo é cadastrado ou editado e possui quantidade maior que uma unidade.

### Pré-requisitos
- Docker instalado e rodando
- Todos os contêineres rodando na máquina deverão estar parados
- Se houver um servidor apache rodando, deverá ser parado, a fim de não conflitar com o docker

### Instalação:
```bash
# Clonar o projeto deste repositório
$ git clone https://github.com/sseduardo13/checklist.git

# Acessar a pasta do projeto
$ cd checklist

# Instalar o projeto
$ docker run --rm --interactive --tty --volume $PWD:/app composer install

# Renomear o arquivo .env.example
$ mv .env.example .env

# Renomear o arquivo .env.testing.example
$ mv .env.testing.example .env.testing

# Fazer o build
$ vendor/bin/sail build

# Executar o docker (sail)
$ vendor/bin/sail up
```

Em outra aba do terminal executar:

```bash
# Gerar a chave encriptada para o projeto
$ vendor/bin/sail artisan key:generate

# Aplicar configurações do arquivo .env
$ vendor/bin/sail artisan config:cache

# Criar tabelas no banco de dados
$ vendor/bin/sail artisan migrate

# Criar tabelas no banco de dados de testes
$ vendor/bin/sail artisan migrate --database=testing

# Popular tabelas no banco de dados
$ vendor/bin/sail artisan db:seed
```

Manter aberto o terminal durante a execução, para finalizar a execução utilize as teclas `Ctrl + C`.

### Execução:
A cada nova execução deverá ser utilizado o comando abaixo dentro da pasta do projeto

```bash
 $ vendor/bin/sail up
```

Manter aberto o terminal durante a execução, para finalizar a execução utilize as teclas `Ctrl + C`.

### Acesso:
Para utlização das rotas da API deverá ser utilizado o client Postman onde deverá ser importado o json collection disponível em https://www.getpostman.com/collections/79a83d659aaf004f5bfd

### Execução da fila:
Para interceptar o recebimento do email é aconselhado utilizar o mailtrap (https://mailtrap.io).

Após salvar as credenciais no arquivo .env do projeto, executar o comando:

```bash
$ vendor/bin/sail artisan config:cache
```

Para executar a fila deverá ser utilizado o comando:

```bash
 $ vendor/bin/sail artisan queue:listen
```
Manter aberto o terminal durante a execução, para finalizar a execução utilize as teclas `Ctrl + C`.

### Testes:
Para executar os testes deverá ser utilizado o comando:

```bash
 $ vendor/bin/phpunit
```

### Tecnologias utilizadas
- Laravel: 9
- PHP: 8
- MySql: 8