<?php

use App\Http\Controllers\CakeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(CakeController::class)->group(function () {
    Route::get('cakes', 'showCakes');
    Route::post('cakes', 'addCake');
    Route::put('cakes/{id}', 'updateCake');
    Route::delete('cakes/{id}', 'deleteCake');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
