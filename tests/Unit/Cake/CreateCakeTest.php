<?php

namespace Tests\Unit\Cake;

use App\Services\Cake\Contracts\CreateCakeServiceContract;
use App\Services\Cake\CreateCakeService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateCakeTest extends TestCase
{
    use RefreshDatabase;

    protected CreateCakeServiceContract $createCakeService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->createCakeService = app(CreateCakeService::class);
        $this->faker = Factory::create();
    }

    public function testCreateCakeSuccess()
    {
        $nameFaker = $this->faker->word();
        $weightFaker = $this->faker->numberBetween(100, 2000);
        $valueFaker = $this->faker->numberBetween(10, 999);
        $quantityFaker = $this->faker->numberBetween(1, 50);

        $cake = $this->createCakeService->create([
            'name' => $nameFaker,
            'weight' => $weightFaker,
            'value' => $valueFaker,
            'quantity' => $quantityFaker
        ]);

        $this->assertEquals($nameFaker, $cake->name);
        $this->assertEquals($weightFaker, $cake->weight);
        $this->assertEquals($valueFaker, $cake->value);
        $this->assertEquals($quantityFaker, $cake->quantity);
    }

    public function testCreateCakeFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->createCakeService->create([
            'name' => true,
            'weight' => '',
            'value' => null,
            'quantity' => 'abc'
        ]);
    }

}
