<?php

namespace Tests\Unit\Cake;

use App\Models\Cake;
use App\Services\Cake\Contracts\UpdateCakeServiceContract;
use App\Services\Cake\UpdateCakeService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateCakeTest extends TestCase
{
    use RefreshDatabase;

    protected UpdateCakeServiceContract $updateCakeService;
    protected Generator $faker;
    protected Cake $cake;

    public function setUp(): void
    {
        parent::setUp();

        $this->updateCakeService = app(UpdateCakeService::class);
        $this->faker = Factory::create();
        $this->cake = Cake::factory()->create();
    }

    public function testUpdateCakeSuccess()
    {
        $nameFaker = $this->faker->company();
        $weightFaker = $this->faker->numberBetween(100, 2000);
        $valueFaker = $this->faker->numberBetween(10, 999);
        $quantityFaker = $this->faker->numberBetween(1, 50);

        $cake = $this->updateCakeService->update([
            'name' => $nameFaker,
            'weight' => $weightFaker,
            'value' => $valueFaker,
            'quantity' => $quantityFaker
        ], $this->cake->id);

        $this->assertEquals($nameFaker, $cake->name);
        $this->assertEquals($weightFaker, $cake->weight);
        $this->assertEquals($valueFaker, $cake->value);
        $this->assertEquals($quantityFaker, $cake->quantity);
    }

    public function testUpdateCakeFailed()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);

        $this->updateCakeService->update([
            'name' => true,
            'weight' => '',
            'value' => null,
            'quantity' => 'abc'
        ], $this->cake->id);
    }
}