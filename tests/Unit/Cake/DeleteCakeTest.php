<?php

namespace Tests\Unit\Cake;

use App\Models\Cake;
use App\Services\Cake\Contracts\DeleteCakeServiceContract;
use App\Services\Cake\DeleteCakeService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteCakeTest extends TestCase
{
    use RefreshDatabase;

    protected DeleteCakeServiceContract $deleteCakeService;
    protected Generator $faker;
    protected Cake $cake;

    public function setUp(): void
    {
        parent::setUp();

        $this->deleteCakeService = app(DeleteCakeService::class);
        $this->faker = Factory::create();
    }

    public function testDeleteCakeSuccess()
    {
        $cake = Cake::factory()->create();

        $this->assertEquals(1, $this->deleteCakeService->delete($cake->id));
    }

    public function testDeleteCakeFailed()
    {
        $this->assertEquals(0, $this->deleteCakeService->delete(999));
    }
}