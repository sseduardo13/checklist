<?php

namespace Tests\Unit\Cake;

use App\Models\Cake;
use App\Services\Cake\Contracts\FindCakeServiceContract;
use App\Services\Cake\FindCakeService;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FindCakeTest extends TestCase
{
    use RefreshDatabase;

    protected FindCakeServiceContract $findCakeService;
    protected Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->findCakeService = app(FindCakeService::class);
        $this->faker = Factory::create();
    }

    /**
     * @throws \Exception
     */
    public function testFindAllCakes()
    {
        Cake::factory(2)->create();

        $cakes = $this->findCakeService->findAll();

        $this->assertNotNull($cakes);
        $this->assertEquals(2, $cakes->count());
    }
}