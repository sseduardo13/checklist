<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Interested extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'cake_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return BelongsTo
     */
    public function cake(): BelongsTo
    {
        return $this->belongsTo(Cake::class);
    }
}
