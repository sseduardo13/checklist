<?php

namespace App\Http\Controllers;

use App\Http\Requests\CakeRequest;
use App\Http\Resources\CakeResource;
use App\Http\Resources\CakesCollection;
use App\Http\Resources\DeleteCakeResource;
use App\Services\Cake\Contracts\CreateCakeServiceContract;
use App\Services\Cake\Contracts\DeleteCakeServiceContract;
use App\Services\Cake\Contracts\FindCakeServiceContract;
use App\Services\Cake\Contracts\UpdateCakeServiceContract;
use App\Services\Interested\Contracts\CreateInterestedServiceContract;

class CakeController extends Controller
{
    /** 
     * @var CreateCakeServiceContract
     */
    private CreateCakeServiceContract $createCakeService;
    
    /** 
     * @var CreateInterestedServiceContract
     */
    private CreateInterestedServiceContract $createInterestedService;

    /** 
     * @var DeleteCakeServiceContract
     */
    private DeleteCakeServiceContract $deleteCakeService;

    /** 
     * @var FindCakeServiceContract
     */
    private FindCakeServiceContract $findCakeService;
  
    /** 
     * @var UpdateCakeServiceContract
     */
    private UpdateCakeServiceContract $updateCakeService;

    
    /**
     * @param CreateCakeServiceContract $createCakeService
     * @param CreateInterestedServiceContract $createInterestedService
     * @param DeleteCakeServiceContract $deleteCakeService
     * @param FindCakeServiceContract $findCakeService
     * @param UpdateCakeServiceContract $updateCakeService
     */
    public function __construct(
        CreateCakeServiceContract $createCakeService,
        CreateInterestedServiceContract $createInterestedService,
        DeleteCakeServiceContract $deleteCakeService,
        FindCakeServiceContract $findCakeService,
        UpdateCakeServiceContract $updateCakeService
    )
    {
        $this->createCakeService = $createCakeService;
        $this->createInterestedService = $createInterestedService;
        $this->deleteCakeService = $deleteCakeService;
        $this->findCakeService = $findCakeService;
        $this->updateCakeService = $updateCakeService;
    }


    /**
     * Display all resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCakes()
    {
        try {
            return (new CakesCollection($this->findCakeService->findAll()));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CakeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function addCake(CakeRequest $request)
    {
        try {
            $cake = $this->createCakeService->create($request->all());
            $this->createInterestedService->create($request->interesteds, $cake->id);
            return (new CakeResource($cake));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CakeRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateCake(CakeRequest $request, int $id)
    {
        try {
            return (new CakeResource($this->updateCakeService->update($request->all(), $id)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCake(int $id)
    {
        try {
            return (new DeleteCakeResource($this->deleteCakeService->delete($id)));
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'data' => $exception->getMessage(),
                'code' => $exception->getCode()
            ]);
        }
    }
}
