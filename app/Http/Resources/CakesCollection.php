<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CakesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($cake){
                return [
                    'id' => $cake->id,
                    'nome' => $cake->name,
                    'peso' => $cake->weight,
                    'valor' => $cake->value,
                    'quantidade' => $cake->quantity
                ];
            }),
        ];
    }
}
