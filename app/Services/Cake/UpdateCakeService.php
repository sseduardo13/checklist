<?php

namespace App\Services\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\UpdateCakeRepository;
use App\Services\Cake\Contracts\UpdateCakeServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateCakeService implements UpdateCakeServiceContract
{
    /**
     * @var UpdateCakeRepository
     */
    protected UpdateCakeRepository $updateCakeRepository;

    /**
     * @param UpdateCakeRepository $updateCakeRepository
     */
    public function __construct(UpdateCakeRepository $updateCakeRepository)
    {
        $this->updateCakeRepository = $updateCakeRepository;
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Exception|Cake
     * @throws Exception
     */
    public function update(array $attributes, int $id): Exception|Cake
    {
        try {
            return $this->updateCakeRepository->update($attributes, $id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
