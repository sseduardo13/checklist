<?php

namespace App\Services\Cake;

use App\Repositories\Cake\Contracts\FindAllCakesRepository;
use App\Repositories\Cake\Contracts\FindCakeByIdRepository;
use App\Services\Cake\Contracts\FindCakeServiceContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindCakeService implements FindCakeServiceContract
{
    /**
     * @var FindAllCakesRepository
     */
    protected FindAllCakesRepository $findAllCakesRepository;

    /**
     * @var FindCakeByIdRepository
     */
    protected FindCakeByIdRepository $findCakeByIdRepository;

    /**
     * @param FindAllCakesRepository $findAllCakesRepository
     * @param FindCakeByIdRepository $findCakeByIdRepository
     */
    public function __construct(
        FindAllCakesRepository $findAllCakesRepository,
        FindCakeByIdRepository $findCakeByIdRepository
    )
    {
        $this->findAllCakesRepository = $findAllCakesRepository;
        $this->findCakeByIdRepository = $findCakeByIdRepository;
    }

    /**
     * @return Collection|Exception|null
     * @throws Exception
     */
    public function findAll(): Collection|Exception|null
    {
        try {
            return $this->findAllCakesRepository->findAll();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }

    /**
     * @param int $id
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $id): Model|Exception|null
    {
        try {
            return $this->findCakeByIdRepository->find($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}

