<?php

namespace App\Services\Cake\Contracts;

use Exception;

interface DeleteCakeServiceContract
{
    /**
     * @param $id
     * @return Exception|int
     * @throws Exception
     */
    public function delete(int $id): Exception|int|null;
}
