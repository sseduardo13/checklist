<?php

namespace App\Services\Cake\Contracts;

use App\Models\Cake;
use Exception;

interface UpdateCakeServiceContract
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Exception|Cake
     * @throws Exception
     */
    public function update(array $attributes, int $id): Exception|Cake;
}
