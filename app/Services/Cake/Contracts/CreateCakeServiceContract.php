<?php

namespace App\Services\Cake\Contracts;

use App\Models\Cake;
use Exception;

interface CreateCakeServiceContract
{
    /**
     * @param array $attributes
     * @return Cake|Exception
     * @throws Exception
     */
    public function create(array $attributes): Cake|Exception;
}
