<?php

namespace App\Services\Cake\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface FindCakeServiceContract
{
    /**
    * @return Collection|Exception|null
    * @throws Exception
    */
    public function findAll(): Collection|Exception|null;

    /**
     * @param int $id
     * @return Model|Exception|null
     * @throws Exception
     */
    public function find(int $id): Model|Exception|null;
}
