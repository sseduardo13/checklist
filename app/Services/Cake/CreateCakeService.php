<?php

namespace App\Services\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\CreateCakeRepository;
use App\Services\Cake\Contracts\CreateCakeServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateCakeService implements CreateCakeServiceContract
{
    /**
     * @var CreateCakeRepository
     */
    protected CreateCakeRepository $createCakeRepository;

    /**
     * @param CreateCakeRepository $createCakeRepository
     */
    public function __construct(CreateCakeRepository $createCakeRepository)
    {
        $this->createCakeRepository = $createCakeRepository;
    }

    /**
     * @param array $attributes
     * @return Cake|Exception
     * @throws Exception
     */
    public function create(array $attributes): Cake|Exception
    {
        try {
            return $this->createCakeRepository->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
