<?php

namespace App\Services\Cake;

use App\Repositories\Cake\Contracts\DeleteCakeRepository;
use App\Services\Cake\Contracts\DeleteCakeServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class DeleteCakeService implements DeleteCakeServiceContract
{
    /**
     * @var DeleteCakeRepository
     */
    protected DeleteCakeRepository $deleteCakeRepository;

    /**
     * @param DeleteCakeRepository $deleteCakeRepository
     */
    public function __construct(DeleteCakeRepository $deleteCakeRepository)
    {
        $this->deleteCakeRepository = $deleteCakeRepository;
    }

    /**
     * @param $id
     * @return Exception|int
     * @throws Exception
     */
    public function delete(int $id): Exception|int|null
    {
        try {
            return $this->deleteCakeRepository->delete($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
