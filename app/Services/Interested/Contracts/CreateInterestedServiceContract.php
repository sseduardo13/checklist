<?php

namespace App\Services\Interested\Contracts;

use App\Models\Interested;
use Exception;

interface CreateInterestedServiceContract
{
    /**
     * @param array $interesteds
     * @param int $cakeId
     * @return bool|Exception
     * @throws Exception
     */
    public function create(array $interesteds, int $cakeId): bool|Exception;
}
