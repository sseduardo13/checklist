<?php

namespace App\Services\Interested;

use App\Models\Interested;
use App\Repositories\Interested\Contracts\CreateInterestedRepository;
use App\Services\Interested\Contracts\CreateInterestedServiceContract;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateInterestedService implements CreateInterestedServiceContract
{
    /**
     * @var CreateInterestedRepository
     */
    protected CreateInterestedRepository $createInterestedRepository;

    /**
     * @param CreateInterestedRepository $createInterestedRepository
     */
    public function __construct(CreateInterestedRepository $createInterestedRepository)
    {
        $this->createInterestedRepository = $createInterestedRepository;
    }

    /**
     * @param array $interesteds
     * @param int $cakeId
     * @return bool|Exception
     * @throws Exception
     */
    public function create(array $interesteds, int $cakeId): bool|Exception
    {
        try {
            foreach ($interesteds as $interestd) {
                $this->createInterestedRepository->create([
                    'email' => $interestd,
                    'cake_id' => $cakeId
                ]);
            }
            return true;
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
