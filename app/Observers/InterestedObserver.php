<?php

namespace App\Observers;

use App\Jobs\SendInterestedEmail;
use App\Models\Cake;
use App\Models\Interested;
use App\Services\Cake\Contracts\FindCakeServiceContract;

class InterestedObserver
{
    /**
     * Handle the Interested "created" event.
     *
     * @param  \App\Models\Interested  $interested
     * @return void
     */
    public function created(Interested $interested)
    {
        $cake = app(FindCakeServiceContract::class)->find($interested->cake_id);
        
        if ($cake->quantity > 0) {
            $job = new SendInterestedEmail($cake, $interested);
            dispatch($job);
        }
    }

}
