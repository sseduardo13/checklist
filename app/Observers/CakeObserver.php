<?php

namespace App\Observers;

use App\Jobs\SendInterestedEmail;
use App\Models\Cake;

class CakeObserver
{
    /**
     * Handle the Cake "updated" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function updated(Cake $cake)
    {
        if ($cake->quantity > 0) {
            foreach ($cake->interesteds as $interested) {
                $job = new SendInterestedEmail($cake, $interested);
                dispatch($job);
            }
        }
    }

}
