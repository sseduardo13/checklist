<?php

namespace App\Repositories\Interested\Contracts;

use App\Models\Interested;
use Exception;

interface CreateInterestedRepository
{    
    /**
     * @param array $attributes
     * @return Interested|Exception
     * @throws Exception
     */
    public function create(array $attributes): Interested|Exception;
}
