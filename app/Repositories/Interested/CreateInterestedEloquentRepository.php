<?php

namespace App\Repositories\Interested;

use App\Models\Interested;
use App\Repositories\Interested\Contracts\CreateInterestedRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateInterestedEloquentRepository implements CreateInterestedRepository
{
    /**
     * @var Model|Interested
     */
    private Model|Interested $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Interested();
    }

    /**
     * @param array $attributes
     * @return Interested|Exception
     * @throws Exception
     */
    public function create(array $attributes): Interested|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
