<?php

namespace App\Repositories\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\DeleteCakeRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class DeleteCakeEloquentRepository implements DeleteCakeRepository
{
    /**
     * @var Model|Cake
     */
    private Model|Cake $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Cake();
    }

    /**
     * @param int $id
     * @return int|Exception|null
     * @throws Exception
     */
    public function delete(int $id): int|Exception|null
    {
        try {
            return $this->eloquentModel->destroy($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
