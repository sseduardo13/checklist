<?php

namespace App\Repositories\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\FindCakeByIdRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindCakeByIdEloquentRepository implements FindCakeByIdRepository
{
    /**
     * @var Model|Cake
     */
    private Model|Cake $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Cake();
    }

    /**
     * @param int $id
     * @return Model|Exception|null
     */
    public function find(int $id): Model|Exception|null
    {
        try {
            return $this->eloquentModel->find($id);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}
