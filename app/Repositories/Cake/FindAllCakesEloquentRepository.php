<?php

namespace App\Repositories\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\FindAllCakesRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FindAllCakesEloquentRepository implements FindAllCakesRepository
{
    /**
     * @var Model|Cake
     */
    private Model|Cake $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Cake();
    }

    /**
     * @return Collection|Exception|null
     */
    public function findAll(): Collection|Exception|null
    {
        try {
            return $this->eloquentModel->all();
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            return $exception;
        }
    }
}
