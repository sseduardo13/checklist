<?php

namespace App\Repositories\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\UpdateCakeRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UpdateCakeEloquentRepository implements UpdateCakeRepository
{
    /**
     * @var Model|Cake
     */
    private Model|Cake $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Cake();
    }

    /**
     * @param array $attributes
     * @param int $id
     * @return Cake|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Cake|Exception
    {
        try {

            $model = $this->eloquentModel->findOrFail($id);
            $model->fill($attributes);
            $model->save();

            return $model;

        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
