<?php

namespace App\Repositories\Cake;

use App\Models\Cake;
use App\Repositories\Cake\Contracts\CreateCakeRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CreateCakeEloquentRepository implements CreateCakeRepository
{
    /**
     * @var Model|Cake
     */
    private Model|Cake $eloquentModel;

    public function __construct()
    {
        $this->eloquentModel = new Cake();
    }

    /**
     * @param array $attributes
     * @return Cake|Exception
     * @throws Exception
     */
    public function create(array $attributes): Cake|Exception
    {
        try {
            return $this->eloquentModel->create($attributes);
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
            throw $exception;
        }
    }
}
