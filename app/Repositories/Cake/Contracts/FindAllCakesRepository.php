<?php

namespace App\Repositories\Cake\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Collection;

interface FindAllCakesRepository
{
    /**
     * @return Collection|Exception|null
     */
    public function findAll(): Collection|Exception|null;
}
