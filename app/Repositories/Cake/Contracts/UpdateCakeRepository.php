<?php

namespace App\Repositories\Cake\Contracts;

use App\Models\Cake;
use Exception;

interface UpdateCakeRepository
{
    /**
     * @param array $attributes
     * @param int $id
     * @return Cake|Exception
     * @throws Exception
     */
    public function update(array $attributes, int $id): Cake|Exception;
}
