<?php

namespace App\Repositories\Cake\Contracts;

use Exception;

interface DeleteCakeRepository
{
    /**
     * @param int $id
     * @return int|Exception|null
     * @throws Exception
     */
    public function delete(int $id): int|Exception|null;
}
