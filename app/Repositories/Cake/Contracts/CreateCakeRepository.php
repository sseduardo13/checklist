<?php

namespace App\Repositories\Cake\Contracts;

use App\Models\Cake;
use Exception;

interface CreateCakeRepository
{    
    /**
     * @param array $attributes
     * @return Cake|Exception
     * @throws Exception
     */
    public function create(array $attributes): Cake|Exception;
}
