<?php

namespace App\Repositories\Cake\Contracts;

use Exception;
use Illuminate\Database\Eloquent\Model;

interface FindCakeByIdRepository
{
    /**
     * @param int $id
     * @return Model|Exception|null
     */
    public function find(int $id): Model|Exception|null;
}
