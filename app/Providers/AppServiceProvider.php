<?php

namespace App\Providers;

use App\Models\Cake;
use App\Models\Interested;
use App\Observers\CakeObserver;
use App\Observers\InterestedObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindObservers();
    }

    /**
     * Bind Model Observers
     */
    private function bindObservers()
    {
        Cake::observe(CakeObserver::class);
        Interested::observe(InterestedObserver::class);
    }
}
