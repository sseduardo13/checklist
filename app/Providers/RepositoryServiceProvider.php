<?php

namespace App\Providers;

use App\Repositories\Cake\Contracts\CreateCakeRepository;
use App\Repositories\Cake\Contracts\DeleteCakeRepository;
use App\Repositories\Cake\Contracts\FindAllCakesRepository;
use App\Repositories\Cake\Contracts\FindCakeByIdRepository;
use App\Repositories\Cake\Contracts\UpdateCakeRepository;
use App\Repositories\Cake\CreateCakeEloquentRepository;
use App\Repositories\Cake\DeleteCakeEloquentRepository;
use App\Repositories\Cake\FindAllCakesEloquentRepository;
use App\Repositories\Cake\FindCakeByIdEloquentRepository;
use App\Repositories\Cake\UpdateCakeEloquentRepository;
use App\Repositories\Interested\Contracts\CreateInterestedRepository;
use App\Repositories\Interested\CreateInterestedEloquentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindCake();
        $this->bindInterested();
    }

    public function bindCake()
    {
        $this->app->bind(CreateCakeRepository::class, CreateCakeEloquentRepository::class);
        $this->app->bind(DeleteCakeRepository::class, DeleteCakeEloquentRepository::class);
        $this->app->bind(FindAllCakesRepository::class, FindAllCakesEloquentRepository::class);
        $this->app->bind(FindCakeByIdRepository::class, FindCakeByIdEloquentRepository::class);
        $this->app->bind(UpdateCakeRepository::class, UpdateCakeEloquentRepository::class);
    }
    
    public function bindInterested()
    {
        $this->app->bind(CreateInterestedRepository::class, CreateInterestedEloquentRepository::class);
    }

}
