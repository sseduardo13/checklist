<?php

namespace App\Providers;

use App\Services\Cake\Contracts\CreateCakeServiceContract;
use App\Services\Cake\Contracts\DeleteCakeServiceContract;
use App\Services\Cake\Contracts\FindCakeServiceContract;
use App\Services\Cake\Contracts\UpdateCakeServiceContract;
use App\Services\Cake\CreateCakeService;
use App\Services\Cake\DeleteCakeService;
use App\Services\Cake\FindCakeService;
use App\Services\Cake\UpdateCakeService;
use App\Services\Interested\Contracts\CreateInterestedServiceContract;
use App\Services\Interested\CreateInterestedService;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindCake();
        $this->bindInterested();
    }

    public function bindCake()
    {
        $this->app->bind(CreateCakeServiceContract::class, CreateCakeService::class);
        $this->app->bind(DeleteCakeServiceContract::class, DeleteCakeService::class);
        $this->app->bind(FindCakeServiceContract::class, FindCakeService::class);
        $this->app->bind(UpdateCakeServiceContract::class, UpdateCakeService::class); 
    }
    
    public function bindInterested()
    {
        $this->app->bind(CreateInterestedServiceContract::class, CreateInterestedService::class);
    }

}
