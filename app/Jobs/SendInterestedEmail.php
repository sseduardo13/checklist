<?php

namespace App\Jobs;

use App\Mail\InterestedMail;
use App\Models\Cake;
use App\Models\Interested;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendInterestedEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $cake;
    private $interested;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Cake $cake, Interested $interested)
    {
        $this->cake = $cake;
        $this->interested = $interested;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->interested)->send(new InterestedMail($this->cake));
    }
}
